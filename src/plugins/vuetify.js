/** @format */

import Vue from "vue";
import Vuetify from "vuetify/lib";
import es from "vuetify/es5/locale/es";
import "vuetify/dist/vuetify.min.css";
import "@mdi/font/css/materialdesignicons.css"; // Ensure you are using css-loader
let primary = process.env.VUE_APP_COLOR;

Vue.use(Vuetify);
const opts = {
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: primary,
        secondary: "#424242",
        accent: "#82B1FF",
        error: "#FF5252",
        info: "#00bcd4",
        success: "#4CAF50",
        warning: "#FFC107",
      },
    },
  },
  lang: {
    locales: { es },
    current: "es",
  },
  icons: {
    iconfont: "mdi", // default - only for display purposes
  },
};
export default new Vuetify(opts);
