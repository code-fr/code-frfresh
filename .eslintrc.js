module.exports = {
	extends: [
		'plugin:vue/recommended' // Use this if you are using Vue.js 2.x.
	],
	rules: {
		// override/add rules settings here, such as:
		'vue/no-unused-vars': 'off',
		'vue/order-in-components': 'off',
		'vue/valid-v-slot':'off'
	}
};
