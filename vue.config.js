const openInEditor = require('launch-editor-middleware');
module.exports = {
  transpileDependencies: ['vuetify'],
  configureWebpack: {
    devServer: {
      before(app) {
        app.use('/__open-in-editor', openInEditor('code'));
      }
    }
  },
  pwa: {
    name: 'fr-fresh-produce',
    themeColor: '#FFFFFF',
    msTileColor: '#FFFFFF',
    manifestOptions: {
      background_color: '#FFFFFF',
      name: 'Fr Fresh Produce',
      short_name: 'fresh',
      display: 'standalone',
      start_url: '.'
    }
  }
};
